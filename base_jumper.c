#include <stdio.h>
#include <stdlib.h>

#include "decbase.h"


char int_to_char(int val) {
	return val + '0';
}

char get_base_char(int base_val) {
	char base_chars[] = {'A','B','C','D','E','F'};
	int reduce_by = 10;
	return base_chars[base_val-reduce_by];
}

struct base* dec_to_base(int dec, int base_val) {
	int rem_val;

	struct base *pbase = NULL;
	while(dec > 0) {
		if (pbase == NULL) {	// Initialize structure pointer
			pbase = (struct base*) malloc(sizeof(struct base));
			pbase->next = NULL;
		} else {	// Append new structure pointer
			struct base *temp_pbase = (struct base*) malloc(sizeof(struct base));
			temp_pbase->next = NULL;

			temp_pbase->next = pbase;
			pbase = temp_pbase;
		}

		rem_val = dec%base_val;
		char rem_base = ' ';
		/* Acquire 'char' Base value */
		if (rem_val >= 10) {
			rem_base = get_base_char(rem_val);
		} else { // Less than 10
			rem_base = int_to_char(rem_val);
		}

		pbase->base_val = rem_base;
		dec = dec/base_val;
	}

	return pbase;
}

void free_base(struct base *val) {
	struct base *temp_pval;
	while(val != NULL) {
		temp_pval = val;
		val = val->next;
		free(temp_pval);
	}	
}

int main(void)
{
	puts("Welcome to Base Converter");
	puts("Please enter the decimal value you want converted: ");
	int value;
	scanf("%d", &value);	

	int base_value;
	puts("Base you want converted (2-16): ");
	scanf("%d", &base_value);
	if (base_value < 2 && base_value > 16) {
		puts("ERROR: Invalid base input");
		return 1;
	}

	struct base *base_val = dec_to_base(value, base_value);
	printf("This is the converted decimal to base %d: ", base_value);

	/* Print converted value */
	struct base *beg_base_val = base_val; // Save beginning pointer to free later
	while(base_val != NULL) {	// iterate structure
		printf("%c", base_val->base_val);
		base_val = base_val->next;
	}
	printf("\n");

	free_base(beg_base_val);
	
}

